using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Globalization;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.IO;

namespace SI
{
    class DataFunction
    //Public NotInheritable Class DataFunction
    {
        public DataFunction()
        {

        }



        public static string FormatDate(string vDate)
        {
            try
            {
                return DateTime.Parse(vDate).ToString("MM/dd/yyyy");
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static DateTime ToDate(string vDate)
        {
            try
            {
                return DateTime.Parse(vDate);
            }
            catch (Exception ex)
            {
                return DateTime.Today;
                //return null;

            }
        }

        public static double ToAmount(string vAmount)
        {
            try
            {
                if (vAmount.Length <= 0)
                {
                    vAmount = "0";
                }
                return double.Parse(vAmount);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int ToInt(string vInt)
        {
            if (vInt == "")
            {
                return 0;
            }
            int vAmt = 0;
            try
            {
                vAmt = int.Parse(vInt);
                return vAmt;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public static string ValidateAmount(string vAmount)
        {
            if (vAmount == "")
            {
                return "0";
            }
            double vAmt = 0;
            try
            {
                vAmt = DataFunction.ToAmount(vAmount);
                return vAmt.ToString("#,##0.00");
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public static string ValidateString(string vString)
        {
            if (vString == "")
            {
                return "";
            }
            string vStr = "";
            try
            {
                vStr = vString.ToString() + "";
                return vStr;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string ValidateLong(string vAmount)
        {
            if (vAmount == "")
            {
                return "0";
            }
            double vAmt = 0;
            try
            {
                vAmt = long.Parse(vAmount);
                return vAmt.ToString();
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public static bool IsDate(string vDate)
        {
            DateTime cDate;
            try
            {
                cDate = DateTime.Parse(vDate);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool IsNumeric1(string vStr)
        {
            if (vStr != "")
            {
                double nInt = 0;
                try
                {
                    nInt = DataFunction.ToAmount(vStr);
                    return true;
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Invalid entry. Please enter numeric values!", Application.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            return true;
        }

        public static bool IsNumeric2(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }


        //public static string GetFirstWords(string input, int numberWords)
        //{
        //    try
        //    {
        //        // Number of words we still want to display.
        //        int words = numberWords;
        //        // Loop through entire summary.
        //        for (int i = 0; i < input.Length; i++)
        //        {
        //            // Increment words on a space.
        //            if (input[i] == ' ')
        //            {
        //                words--;
        //            }
        //            // If we have no more words to display, return the substring.
        //            if (words == 0)
        //            {
        //                return input.Substring(0, i);
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        // Log the error.
        //    }
        //    return string.Empty;
        //}

        public static string SelectYourWord(string StrText, int CountWord)
        {
            int i = 0;
            bool isCount = false;

            //if (CountWord > 0)
            //{
                foreach (string word in Regex.Split(StrText, @"\s+"))
                {
                    i++;
                    if (i == CountWord)
                    {
                        isCount = true;
                        Console.WriteLine("{0}", word);
                        StrText = word;
                        
                    }
                    //else
                    //{
                    //    StrText = word;
                    //}
                    
                }
            //}

                if (isCount == false)
                {
                    StrText = string.Empty;
                }
            //else
            //{
            //    Console.WriteLine(StrText);
            //}

            return StrText.Trim();
        }

        public static string Ordinal(int num)
        {
            if (num.ToString().EndsWith("11")) return num.ToString() + "th";
            if (num.ToString().EndsWith("12")) return num.ToString() + "th";
            if (num.ToString().EndsWith("13")) return num.ToString() + "th";
            if (num.ToString().EndsWith("1")) return num.ToString() + "st";
            if (num.ToString().EndsWith("2")) return num.ToString() + "nd";
            if (num.ToString().EndsWith("3")) return num.ToString() + "rd";
            return num.ToString() + "th";
        }


        public static byte[] StreamPDFToByte(string strPath)
        {
            string filename = strPath;
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] byteData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(byteData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return byteData; //return the byte data
        }

        public static void StreamByteToPDF(byte[] byteData, string strPath)
        {
            string filename = strPath;
            FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write);

            //Read block of bytes from stream into the byte array
            fs.Write(byteData, 0, byteData.Length);

            //Close the File Stream
            fs.Close();
        }

    }
}
