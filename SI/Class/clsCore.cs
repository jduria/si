﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace SI
{
    public class clsCore
    {
        private static string _ConnString = ConfigurationManager.ConnectionStrings["AppContext"].ToString();
        public static string ConnString { get { return _ConnString; } }

        private static string _SI_APIServer = ConfigurationSettings.AppSettings["SI_APIserver"].ToString();
        public static string SI_APIServer { get { return _SI_APIServer; } }
    }

}