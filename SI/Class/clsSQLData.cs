﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;

namespace SI
{
    public class clsSQLData : IDisposable
    {
        public void Dispose() { GC.SuppressFinalize(this); }
        public clsSQLData() { }

        //Populate Dropdown List
        public static DataTable DropDownList(string pType, string pfilter)
        {

            DataTable tblReturn = new DataTable();
            using (SqlConnection cn = new SqlConnection(clsCore.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spDropdownList";
                    cmd.Parameters.AddWithValue("@pType", DataFunction.ValidateString(pType));
                    cmd.Parameters.AddWithValue("@pFilter", DataFunction.ValidateString(pfilter));
                    cn.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(tblReturn);
                }
            }
            return tblReturn;
        }

        //Populate Customer Order List

        public static DataTable CustomerOrderList(string pStatus, string DateFilterType, string dtFrom, string dtTo, string dtMonthYear)
        {

            DataTable tblReturn = new DataTable();
            using (SqlConnection cn = new SqlConnection(clsCore.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spCustomerOrderList";
                    cmd.Parameters.AddWithValue("@pStatus", DataFunction.ValidateString(pStatus));
                    cmd.Parameters.AddWithValue("@DateFilterType", DataFunction.ValidateString(DateFilterType));
                    cmd.Parameters.AddWithValue("@dtFrom", dtFrom);
                    cmd.Parameters.AddWithValue("@dtTo", dtTo);
                    cmd.Parameters.AddWithValue("@dtMonthYear", DataFunction.ValidateString(dtMonthYear));

                    cn.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(tblReturn);
                }
            }
            return tblReturn;
        }

        public static void InsertUpdate_Orders( string CustomerOrderID, string CustomerID,string SalesAgentID, string OrderStatusID,
	                                                 string OrderDate, string TotalAmount, string Remarks, string ForMonthOf, string LeadTime,
                                                     string MarkDeliveredDate, string MarkDeliveredByID, string DateCancelled, string ReasonForCancellation,
                                                     string CancelledByID, string Version, string IsBadDebt, string IsBilled, string CreatedByID, 
                                                     string ModifiedByID, string UpdateType

        )
        {

            using (SqlConnection cn = new SqlConnection(clsCore.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = "spInsertUpdate_Orders";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pCustomerOrderID", DataFunction.ToInt(CustomerOrderID));
                    cmd.Parameters.AddWithValue("@pCustomerID", DataFunction.ToInt(CustomerID));
                    cmd.Parameters.AddWithValue("@pSalesAgentID", DataFunction.ToInt(SalesAgentID));
                    cmd.Parameters.AddWithValue("@pOrderStatusID", DataFunction.ToInt(OrderStatusID));
                    cmd.Parameters.AddWithValue("@pOrderDate", DataFunction.ToDate(OrderDate));
                    cmd.Parameters.AddWithValue("@pTotalAmount", DataFunction.ToAmount(TotalAmount));
                    cmd.Parameters.AddWithValue("@pRemarks", DataFunction.ValidateString(Remarks).Replace("'", "’").Replace("\"", "’’"));
                    cmd.Parameters.AddWithValue("@pForMonthOf", DataFunction.ToDate(ForMonthOf));
                    cmd.Parameters.AddWithValue("@pLeadTime", DataFunction.ToAmount(LeadTime));
                    cmd.Parameters.AddWithValue("@pMarkDeliveredDate", DataFunction.ToAmount(MarkDeliveredDate));
                    cmd.Parameters.AddWithValue("@pMarkDeliveredByID", DataFunction.ToInt(MarkDeliveredByID));
                    cmd.Parameters.AddWithValue("@pDateCancelled", DataFunction.ToDate(DateCancelled));
                    cmd.Parameters.AddWithValue("@pReasonForCancellation", DataFunction.ValidateString(ReasonForCancellation).Replace("'", "’").Replace("\"", "’’"));
                    cmd.Parameters.AddWithValue("@pCancelledByID", DataFunction.ToInt(CancelledByID));
                    cmd.Parameters.AddWithValue("@pVersion", DataFunction.ToInt(Version));
                    cmd.Parameters.AddWithValue("@pIsBadDebt", DataFunction.ToInt(IsBadDebt));
                    cmd.Parameters.AddWithValue("@pIsBilled", DataFunction.ToInt(IsBilled));
                    cmd.Parameters.AddWithValue("@pCreatedByID", DataFunction.ToInt(CreatedByID));
                    cmd.Parameters.AddWithValue("@pModifiedByID", DataFunction.ToInt(ModifiedByID));
                    cmd.Parameters.AddWithValue("@UpdateType", DataFunction.ValidateString(UpdateType));

                    cn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
         
        }

    }
}