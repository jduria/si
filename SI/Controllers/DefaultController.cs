﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

namespace SI
{
    public class DefaultController : ApiController
    {
        [HttpGet]
        //http://localhost:22881/api/Default/DropdownList?DropdownType=A&Filter=
        public IEnumerable<DropDownModel> DropdownList(string DropdownType, string Filter)
        {
            DataTable dt = clsSQLData.DropDownList(DropdownType, Filter);
            List<DropDownModel> dd = new List<DropDownModel>();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dd.Add(new DropDownModel
                    {
                        strValues = dr["strValue"].ToString(),
                        strText = dr["strText"].ToString()
                    });
                }

            }
            return dd;
        }
    }
}
