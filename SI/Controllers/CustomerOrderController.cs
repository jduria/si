﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

namespace SI
{
    public class CustomerOrderController : ApiController
    {
        [HttpGet]
        //http://localhost:11241/api/CustomerOrder/OrderList?pStatus=&DateFilterType=&dtFrom=&dtTo=&dtMonthYear
        public IEnumerable<CustomerOrderModel> OrderList(string pStatus, string DateFilterType, string dtFrom, string dtTo, string dtMonthYear)
        {
            DataTable dt = clsSQLData.CustomerOrderList(pStatus, DateFilterType, dtFrom, dtTo, dtMonthYear);
            List<CustomerOrderModel> CO = new List<CustomerOrderModel>();


            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    CO.Add(new CustomerOrderModel
                    {
                        CustomerOrderID = dr["CustomerOrderID"].ToString(),
                        CustomerID = dr["CustomerID"].ToString(),
                        OrderDate = dr["OrderDate"].ToString(),
                        SalesAgentName = dr["SalesAgentName"].ToString(),
                        Cust_Company = dr["Cust_Company"].ToString(),
                        TIN = dr["TIN"].ToString(),
                        TotalAmount = dr["TotalAmount"].ToString(),
                        ForMonthOf = dr["ForMonthOf"].ToString(),
                        CreatedBy = dr["CreatedBy"].ToString()

                    });

                }

            }
            return CO;

        }

        [HttpPost]
        //http://localhost:11241/api/CustomerOrder/InsertUpdate_Orders
        public string InsertUpdate_Orders(CustomerOrderModel model)
        {

            string rtn = "Success!";
            try
            {
                clsSQLData.InsertUpdate_Orders( model.CustomerOrderID, model.CustomerID, model.SalesAgentID, model.OrderStatusID,
                                                model.OrderDate, model.TotalAmount, model.Remarks, model.ForMonthOf, model.LeadTime,
                                                model.MarkDeliveredDate, model.MarkDeliveredByID, model.DateCancelled, model.ReasonForCancellation,
                                                model.CancelledByID, model.Version, model.IsBadDebt, model.IsBilled, model.CreatedByID,
                                                model.ModifiedByID, model.UpdateType
                                                );

            }
            catch (Exception ex)
            {
                rtn = ex.Message.ToString();
            }


            return rtn;
        }
    }
}
