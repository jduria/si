(function ($) {
            var kendo = window.kendo,
                ui = kendo.ui,
                Widget = ui.Widget,
                proxy = $.proxy,
                CHANGE = "change",
                PROGRESS = "progress",
                ERROR = "error",
                NS = ".generalInfo";

            var MaskedTimePicker = Widget.extend({
                init: function (element, options) {
                    var that = this;
                    Widget.fn.init.call(this, element, options);

                    $(element).kendoMaskedTextBox({ mask: that.options.timeOptions.mask || "00:00" })
                    .kendoTimePicker({
                        format: that.options.timeOptions.format || "hh:mm",
                        parseFormats: that.options.timeOptions.parseFormats || ["hh:mm", "00:00"]
                    })
                    .closest(".k-timepicker")
                    .add(element)
                    .removeClass("k-textbox");

                    that.element.data("kendoTimePicker").bind("change", function () {
                        that.trigger(CHANGE);
                    });
                },
                options: {
                    name: "MaskedTimePicker",
                    timeOptions: {}
                },
                events: [
                  CHANGE
                ],
                destroy: function () {
                    var that = this;
                    Widget.fn.destroy.call(that);

                    kendo.destroy(that.element);
                },
                value: function (value) {
                    var timepicker = this.element.data("kendoTimePicker");

                    if (value === undefined) {
                        return timepicker.value();
                    }

                    timepicker.value(value);
                }
            });

            ui.plugin(MaskedTimePicker);

        })(window.kendo.jQuery);