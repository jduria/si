﻿function fnDropDowns(ElementID, DropdownType, Filter) {
    //$(document).ready(function () {

    var dropdown = $(ElementID).kendoDropDownList();

    dropdown.data('kendoDropDownList').destroy();
    dropdown.empty();

    var dataSource = new kendo.data.DataSource({
        //data: ds,
        //serverFiltering: true,
        transport: {
            read: function Loadata(options) {
                $.ajax({
                    type: "GET",
                    url: "/api/Default/DropdownList?",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: "DropdownType=" + DropdownType + "&Filter=" + Filter,
                    success: function (data) {
                        options.success(data);

                    }

                });
            }
        }
    });

    dropdown.kendoDropDownList({
        filter: "startswith",
        dataTextField: "strText",
        dataValueField: "strValues",
        optionLabel: "--Please Select--",
        dataSource: dataSource
        //dataBound: function (e) {

        //    setTimeout(function () {

        //        var dropDownCount = dropdown.data("kendoDropDownList");
        //        var len = dropDownCount.dataSource.data().length;

        //        if (len == 2) {

        //            dropDownCount.select(1);
        //        }

        //    }, 300);

        //}
    });

}

function filterMenu(e) {
    if (e.field == "DateReceived" || e.field == "DatePS_Approval" || e.field == "TransactionDate" || e.field == "EndDate") {
        var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
        beginOperator.value("gte");
        beginOperator.trigger("change");

        var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
        endOperator.value("lte");
        endOperator.trigger("change");
        debugger;
        e.container.find(".k-dropdown").hide()
    }
}

function exportReplacenewLine(e) {

    var regex = /\\n/g;
    var sheet = e.workbook.sheets[0];

    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
        var row = sheet.rows[rowIndex];
        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {

            if (kendo.stringify(row.cells[cellIndex].value).search(regex) > 0) {
                //row.cells[cellIndex].value = exportReplacenewLine(row.cells[cellIndex].value);

                return row.cells[cellIndex].value = kendo.stringify(row.cells[cellIndex].value).replace(/"/g, "").replace(regex, " ");
            }
        }
    }

    //var replace_value = value;
    //var regex = /\\n/g; 
    //replace_value = kendo.stringify(replace_value).replace(/"/g, "");

    //return replace_value.replace(regex, " ");
}

function starttimeEditor(container, options) {
    //$('<input id="start" data-text-field="' + options.field + '" data-value-field="' + options.field + '" required data-required-msg="Select start time" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
    $('<input name="' + options.field + '" required data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoMaskedTimePicker({
            });
}

function endtimeEditor(container, options) {
    // $('<input id="end" data-text-field="' + options.field + '" data-value-field="' + options.field + '" required data-required-msg="Select end time" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
    $('<input name="' + options.field + '" required data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoMaskedTimePicker({
            });
}

function dateTimeEditor(container, options) {
    $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" required  data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
            .appendTo(container)
            .kendoMaskedDatePicker({
            });
}

function textboxEditor(container, options) {
    $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" required data-required-msg="PolicyNo is required"  data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
            .appendTo(container)
            .kendoMaskedTextBox({
                mask: "000-0000000"
            });
}

function textAreaEditor(container, options) {
    $('<textarea class="k-textbox" id="' + options.field + '" input name="' + options.field + '" data-text-field="' + options.field + '" data-value-field="' + options.field + '"   data-bind="value:' + options.field + '" data-format="' + options.format + '" cols="50" rows="2" style=" color:black"/>')
            .appendTo(container)

}