﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SI.Master" AutoEventWireup="true" CodeBehind="Customer_Orders.aspx.cs" Inherits="SI.WebPage.Orders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

 <script type="text/javascript">

     var DateFilterType = "MONTHS";
     var dateObj = new Date();
     var month = dateObj.getUTCMonth() + 1; //months from 1-12
     var day = dateObj.getUTCDate();
     var year = dateObj.getUTCFullYear();

     var YearMonth = year.toString() + month.toString();
     var OrderStatusID = 6;
     var CustomerOrderID;

     window.onload = function () {

         fnDropDowns("#ddMonth", "MONTH", "");
         fnDropDowns("#ddYear", "YEAR", "");
         $("#gvCreateOrders").kendoGrid();

         $("#ddAgents").on('change', function () {

             $("#gvCreateOrders").data("kendoGrid").dataSource.read()
         });

         $("#dtCustomFrom").kendoDatePicker({ format: "MM/dd/yyyy", value: new Date() });
         $("#dtCustomTo").kendoDatePicker({ format: "MM/dd/yyyy", value: new Date() });

         $("#ddMonth").data("kendoDropDownList").value(month);
         $("#ddYear").data("kendoDropDownList").value(year);


        

         CustomerOrdersList('gvCustmerOrdersList', '2', DateFilterType, $("#dtCustomFrom").val(), $("#dtCustomTo").val(), YearMonth);
         CustomerOrdersList('gvCustmerOrdersList_Cancelled', '6', DateFilterType, $("#dtCustomFrom").val(), $("#dtCustomTo").val(), YearMonth);

     };

     $(document).ready(function () {
     
         closeNav();

         $("#tabOrders").kendoTabStrip({
             //tabPosition: "left",
             animation: { open: { effects: "fadeIn" } },
             select: function onSelect(e) {
                 setTimeout(function () {
                     if ($(e.item).find("> .k-link").text() == "CANCELLED ORDERS") {
                         OrderStatusID = 2;
                         $(".CancelAllOrder").css('color', 'blue');
                         $(".CancelAllOrder").html('<span class="k-icon k-i-reset"></span>&nbsp;Revert Orders');
                     }
                     else {
                         OrderStatusID = 6;
                         $(".CancelAllOrder").css('color', 'red');
                         $(".CancelAllOrder").html('<span class="k-icon k-i-cancel"></span>&nbsp;Cancel Orders');
                     }
                 }, 150);
             }

         });
         
         $('input[id=rdMonths]').on('click', function () {

             DateFilterType = 'MONTHS';
             $("input[id=rdCustom]").prop("checked", false);
             $("#divFilterMonth").show();
             $("#divFilterCustom").hide();
             
         });

         $('input[id=rdCustom]').on('click', function () {

             DateFilterType = 'CUSTOM';
             $("input[id=rdMonths]").prop("checked", false);

             $("#divFilterMonth").hide();
             $("#divFilterCustom").show();

         });

        

         $("#btnFilter").click(function (e) {

             //CustomerOrdersList("2", DateFilterType, dtFrom, dtTo, dtMonthYear)
            var YearMonth = $("#ddYear").data("kendoDropDownList").value() + $("#ddMonth").data("kendoDropDownList").value();
           
            CustomerOrdersList('gvCustmerOrdersList', '2', DateFilterType, $("#dtCustomFrom").val(), $("#dtCustomTo").val(), YearMonth);
            CustomerOrdersList('gvCustmerOrdersList_Cancelled', '6', DateFilterType, $("#dtCustomFrom").val(), $("#dtCustomTo").val(), YearMonth);

            e.preventDefault();

         });

         $("#btnCreate").click(function (e) {

             fnDropDowns("#ddAgents", "A", "");
             Createorders();

             var divAddCustomer = $("#divAddCustomer");
           

             divAddCustomer.kendoWindow({
                 width: "90%",
                 height: "80%",

                 title: "ADD ORDERS",
                 visible: false,
                 modal: true,
                 //open: function () {
                 //    $("#ddShowHideColumns").data("kendoDropDownList").open();
                 //},
                 //close: function () {
                 //    //allowSubmit = true;
                 //    //setTimeout(function () { allowSubmit = false; updateDropDown(); }, 300);
                 //},
                 resizable: false,
                 actions: [
                     "Maximize",
                     "Pin",
                     "Close"
                 ]
             }).data("kendoWindow").open().center();
             e.preventDefault();
         });

     });
     
     function CustomerOrdersList(obj, pStatus, DateFilterType, dtFrom, dtTo, dtMonthYear) {

         var grid = $("#" + obj).kendoGrid();
         grid.data("kendoGrid").destroy();
         grid.empty();


         var dataSource = new kendo.data.DataSource({
             //data: ds,
             transport: {
                 read: function Loadata(options) {
                     $.ajax({
                         type: "GET",
                         url: "/api/CustomerOrder/OrderList?",
                         dataType: "json",
                         data: "pStatus=" + pStatus + "&DateFilterType=" + DateFilterType + "&dtFrom=" + dtFrom + "&dtTo=" + dtTo + "&dtMonthYear=" + dtMonthYear,
                         contentType: "application/json; charset=utf-8",
                         success: function (data) {
                             options.success(data);

                         }
                     });
                 },
                 destroy: function (options) {

                     //Time_SaveUpdate(options, "", "C")

                 },
                 update: function (options) {

                     //Time_SaveUpdate(options, "", "B2")
                     //InsertUpdate_Orders(options, "B")

                 },
                 parameterMap: function (options, operation) {
                     if (operation !== "read" && options.models) {
                         return { models: kendo.stringify(options.models) };
                     }
                 },
             },
             schema: {
                 model: {
                     id: "CustomerOrderID",
                     fields: {
                         CustomerID: { type: "string", editable: false },
                         OrderDate: { type: "string", editable: false },
                         SalesAgentName: { type: "string", editable: false },
                         Cust_Company: { type: "string", editable: false },
                         TIN: { type: "string", editable: false },
                         TotalAmount: { type: "number", editable: false },
                         ForMonthOf: { type: "string", editable: false },
                         CreatedBy: { type: "string", editable: false }

                     }
                 }
             },
             pageSize: 25
             //batch: true
         });


         grid.kendoGrid({
             dataSource: dataSource,

             //selectable: "multiple, row",
             pageable: {
                 //numeric: true,
                 refresh: true,
                 pageSizes: false,
                 buttonCount: 5
             },

             //scrollable: {
             //    virtual: true
             //},
             //height: "500px",
             //editable: true,
             //pageable: true,
             //selectable: true,
             //toolbar: ["create"],
             sortable: true,
             resizable: true,
             reorderable: false,
             groupable: true,
             //columnMenu: true,
             filterable: true,
             filterMenuInit: filterMenu,
             change: function (e) {
                 CustomerOrderID = JSON.parse("[" + this.selectedKeyNames().join(",") + "]");

             },

             //dataBound: function () {
             //    $(".checkbox").bind("change", function (e) {
             //        $(e.target).closest("tr").toggleClass("k-state-selected");
             //    });
             //},
             //excelExport: exportReplacenewLine,
             persistSelection: true,
             toolbar: [
                      {
                          template: '<a id="btnCancelAllOrder" class="k-button k-grid-custom-command CancelAllOrder" style="color:red" href="\\#" onclick="OrderStatus_Command(); return false;"><span class="k-icon k-i-cancel"></span>&nbsp;Cancel Orders</a>'
                      }, "excel"//"create", "save", "cancel",
             ],
             //excel: {
             //    fileName: "Time Tracker Report.xlsx",
             //    allPages: true
             //},
             columns:
             [
                 {
                     selectable: true,
                     //title: "<input id='header-chb' class='checkbox' type='checkbox' />",
                     //template: '<input  id="#= CustomerOrderID #"  class="checkbox" type="checkbox" />',
                     //'<input class="k-checkbox" id="#= CustomerID#" type="checkbox" name="#= CustomerID #" data-type="boolean"><label class="k-checkbox-label" for="#= CustomerID#"></label>',
                     width: 35
                 },
                 //{ command: [{ template: '<a id="btnCancelOrder" class="k-button k-grid-custom-command" style="color:red" href="\\#" onclick="populateAXASolDocuments(DocPolicyNo); return false;"><span class="glyphicon glyphicon-ban-circle"></span>&nbsp;Cancel Orders</a>' }], title: "ACTION", width: 120 },
                 { field: "CustomerID", width: 100, title: "CustomerID", filterable: { multi: true, search: true }, hidden: true },
                 { field: "CustomerOrderID", width: 100, title: "CustomerOrderID", filterable: { multi: true, search: true }, hidden: true },
                 { field: "OrderDate", width: 100, title: "ORDER DATE", filterable: { multi: true, search: true } },
                 { field: "SalesAgentName", width: 200, title: "SALES AGENT NAME", filterable: { multi: true, search: true } },
                 { field: "Cust_Company", width: 300, title: "CUSTOMER COMPANY NAME", filterable: { multi: true, search: true } },
                 { field: "TIN", width: 120, title: "TIN NO.", filterable: { multi: true, search: true } },
                 { field: "TotalAmount", width: 100, title: "ORDER AMOUNT", format: "{0:n}", attributes: { style: "text-align:right;" }, filterable: { multi: true, search: true } },
                 { field: "ForMonthOf", width: 100, title: "FOR MONTH OF", filterable: { multi: true, search: true } },
                 { field: "CreatedBy", width: 100, title: "CREATED BY", filterable: { multi: true, search: true } },

                 //{ command: ["destroy"], title: "ACTION", width: 150 }

             ],
             //editable: "popup"
             //editable: true
         }).data("kendoGrid").dataSource.read();
     }

     function Createorders() {
         var grid = $("#gvCreateOrders");

         grid.data("kendoGrid").destroy();
         grid.empty();

         var ds = [
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" },
                    { "OrderDate": new Date(), "Cust_Company": "", "TIN": "", "TotalAmount": "", "ForMonthOf": new Date(), "Remarks": "" }
                ]

         //var ds = []

         var dataSource = new kendo.data.DataSource({
             //data: ds,
             transport: {
                 //read: Loadata(options){},
                 read: function Loadata(options) {

                     options.success(ds);
                     
                 },
                 create: function (options) {
                     //console.log(options);
                     InsertUpdate_Orders(options, "A")
                 },
                 //destroy: function (options) {},
                 //update: function (options) {},
                 parameterMap: function (options, operation) {
                     if (operation !== "read" && options.models) {
                         return { models: kendo.stringify(options.models) };
                     }
                 },
             },
             schema: {
                 model: {
                     id: "CustomerOrderID",
                     fields: {
                         CustomerOrderID: { type: "string", editable: false },
                         CustomerID: { type: "string", editable: false },
                         OrderDate: { type: "date", editable: true },
                         Cust_Company: { type: "string", validation: { required: true} },
                         TIN: { type: "string", editable: false },
                         TotalAmount: { type: "number", validation: { required: true, min: 1 }, editable: true },
                         ForMonthOf: { type: "date", editable: true },
                         Remarks: { type: "string", editable: true },
                         SalesAgentID: { type: "string", editable: false },
                         CreatedByID: { type: "string", editable: false },
                         ModifiedByID: { type: "string", editable: false }
                     }
                 }
             },
             //pageSize: 10,
             batch: true
         });

         grid.kendoGrid({
             dataSource: dataSource, // dataSource,
             pageable: {
                 numeric: false,
                 previousNext: false,
                 messages: {
                     display: "Showing {2} data items"
                 }
             },
             scrollable: {
                 endless: true
             },
             sortable: true,
             resizable: true,
             reorderable: false,
             //filterable: true,
             //filterMenuInit: filterMenu,
             //excelExport: exportReplacenewLine,

             toolbar: ["create", "save", "cancel"],
             //excel: {
             //    fileName: "Time Tracker Report.xlsx",
             //    allPages: true
             //},
             columns:
             [
                
                 { field: "CustomerID", width: 100, title: "CustomerID", hidden: true },
                 { field: "CustomerOrderID", width: 100, title: "CustomerOrderID", hidden: true },
                 {
                     field: "OrderDate", width: 100, title: "ORDER DATE", editor: function dtOrderDate(container, options) {
                         $('<input id="' + options.field + '" name="' + options.field + '" required data-bind="value:' + options.field + '"/>')
                         .appendTo(container)
                         .kendoDatePicker({
                             format: 'MM/dd/yyyy',
                             dateInput: true
                         });
                     }, template: "#= kendo.toString(kendo.parseDate(OrderDate), 'MM/dd/yyyy') #"
                 },
                 {
                     field: "Cust_Company", width: 200, title: "CUSTOMER COMPANY NAME", editor: function ddCompany(container, options) {
                    
                         var dsCompany = new kendo.data.DataSource({
                             //data: ds,
                             transport: {
                                 read: function Loadata(options) {
                                     $.ajax({
                                         type: "GET",
                                         url: SI_APIserver + "/api/Default/DropdownList",
                                         dataType: "json",
                                         contentType: "application/json; charset=utf-8",
                                         data: "DropdownType=C&Filter=" + $("#ddAgents").val(),
                                         success: function (data) {
                                             options.success(data);
                                         }
                                     });
                                 }
                             }
                         });

                         $('<select id="' + options.field + '" name="' + options.field + '" required data-bind="value:' + options.field + '" />')
                         .appendTo(container)
                         .kendoDropDownList({
                             filter: "startswith",
                             autoBind: false,
                             dataValueField: "strValues",
                             dataTextField: "strText",
                             dataSource: dsCompany,
                             select: function (e){},
                             change: function (e) {

                                 var gvCreateOrders = $("#gvCreateOrders").data("kendoGrid"),
                                    model = gvCreateOrders.dataItem(this.element.closest("tr"));
                                 model.CustomerID = this.value();

                                 var gvCreateOrders = $("#gvCreateOrders").data("kendoGrid");
                                 $.ajax({
                                     type: "GET",
                                     url: SI_APIserver + "/api/Default/DropdownList",
                                     dataType: "json",
                                     contentType: "application/json; charset=utf-8",
                                     data: "DropdownType=C2&Filter=" + this.value(),
                                     success: function (data) {
                                         
                                         console.log(data)
                                         model.TIN = data[0].strText;
                                         gvCreateOrders.refresh();
                                         //alert(data.strText);
                                     }
                                 });

                                 model.Cust_Company = this.text();
                                 //model.TIN = "092-3100-242";

                                
                                 gvCreateOrders.refresh();
                                
                             }
                           

                         });
                     }, template: "#=Cust_Company#"
                 },
                 { field: "TIN", width: 100, title: "TIN NO."},
                 { field: "TotalAmount", width: 100, title: "SALES AMOUNT", format: "{0:n}", attributes: { style: "text-align:right;" } },
                 {
                     field: "ForMonthOf", width: 120, title: "FOR MONTH OF",
                     editor: function dtForMonthOf(container, options)
                     {
                         $('<input id="' + options.field + '" name="' + options.field + '" required data-bind="value:' + options.field + '"/>')
                         .appendTo(container)
                         .kendoDatePicker({
                             start: "year",
                             depth: "year",
                             format: "MMMM yyyy",
                             // specifies that DateInput is used for masking the input element
                             dateInput: true
                         });
                     },
                     template: "#= kendo.toString(kendo.parseDate(new Date()), 'MMMM yyyy') #"
                 },
                 { field: "Remarks", width: 150, title: "REMARKS" },
                 { command: ["destroy"], title: "ACTION", width: 100 }

             ],
             //editable: "popup"
             editable: true,
         }).data("kendoGrid").dataSource.read();

     }

     function InsertUpdate_Orders(options, UpdateType)
     {
         $.each(options.data.models, function (key, value) {

             if (value.CustomerID != "") {
                 var dataToPost = {
                     //CustomerOrderID: '1', //options.data.CustomerOrderID,
                     CustomerID: value.CustomerID,
                     SalesAgentID: $("#ddAgents").data("kendoDropDownList").value(), //options.data.SalesAgentID,
                     OrderStatusID: '2',//options.data.OrderStatusID,
                     OrderDate: value.OrderDate,
                     TotalAmount: value.TotalAmount,
                     Remarks: value.Remarks,
                     ForMonthOf: value.ForMonthOf,
                     //LeadTime: options.data.LeadTime,
                     MarkDeliveredDate: options.data.MarkDeliveredDate,
                     MarkDeliveredByID: UserID, //options.data.MarkDeliveredByID,
                     //DateCancelled: options.data.DateCancelled,
                     //ReasonForCancellation: options.data.ReasonForCancellation,
                     CancelledByID: UserID, //options.data.CancelledByID,
                     //Version: options.data.Version,
                     //IsBadDebt: options.data.IsBadDebt,
                     //IsBilled: options.data.IsBilled,
                     CreatedByID: UserID, //options.data.CreatedByID,
                     ModifiedByID: UserID,//options.data.ModifiedByID,
                     UpdateType: UpdateType

                 }

                 $.ajax({
                     type: "POST",
                     url: "/api/CustomerOrder/InsertUpdate_Orders",
                     dataType: "json",
                     contentType: "application/json; charset=utf-8",
                     data: JSON.stringify(dataToPost),
                     success: function (data) {

                        
                     }
                 });

             }
         });

         $("#gvCreateOrders").data("kendoGrid").dataSource.read();
         $("#gvCustmerOrdersList").data("kendoGrid").dataSource.read();


     }

     function OrderStatus_Command() {

         $.each(CustomerOrderID, function (key, value) {
             
                 var dataToPost = {
                     CustomerOrderID: value,
                     //CustomerID: value.CustomerID,
                     //SalesAgentID: $("#ddAgents").data("kendoDropDownList").value(), //options.data.SalesAgentID,
                     OrderStatusID: OrderStatusID,//options.data.OrderStatusID,
                     //OrderDate: value.OrderDate,
                     //TotalAmount: value.TotalAmount,
                     //Remarks: value.Remarks,
                     //ForMonthOf: value.ForMonthOf,
                     //LeadTime: options.data.LeadTime,
                     //MarkDeliveredDate: options.data.MarkDeliveredDate,
                     //MarkDeliveredByID: "1", //options.data.MarkDeliveredByID,
                     //DateCancelled: options.data.DateCancelled,
                     //ReasonForCancellation: options.data.ReasonForCancellation,
                     CancelledByID: UserID, //options.data.CancelledByID,
                     //Version: options.data.Version,
                     //IsBadDebt: options.data.IsBadDebt,
                     //IsBilled: options.data.IsBilled,
                     //CreatedByID: "1", //options.data.CreatedByID,
                     ModifiedByID: UserID,//options.data.ModifiedByID,
                     UpdateType: "B"

                 }

                 $.ajax({
                     type: "POST",
                     url: "/api/CustomerOrder/InsertUpdate_Orders",
                     dataType: "json",
                     contentType: "application/json; charset=utf-8",
                     data: JSON.stringify(dataToPost),
                     success: function (data) {

                         $("#gvCustmerOrdersList").data("kendoGrid").dataSource.read();
                         $("#gvCustmerOrdersList_Cancelled").data("kendoGrid").dataSource.read();
                     }
                 });
         });

       
     }

    
</script>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm">
                <ul class="list-inline">
                    <li>
                        <label for="rdMonths" style="color:#428bca">FILTER:</label>
                    </li>
                    <li>
                        <input type="radio" id="rdMonths" class="k-radio"  checked="checked"/>
                        <label for="rdMonths" class="k-radio-label">Months</label>
                    </li>
                    <li>
                        <input type="radio" id="rdCustom" class="k-radio" />
                        <label for="rdCustom" class="k-radio-label">Custom</label>
                    </li>
                    <li>
                        <div id="divFilterCustom" style="display:none">
                             <ul class="list-inline">
                                 <li>
                                     <input type="text" id="dtCustomFrom" style="width:135px" />
                                 </li>
                                 <li>
                                     <input type="text" id="dtCustomTo" style="width:135px" />
                                 </li>
                             </ul>
                        </div>

                        <div id="divFilterMonth">
                             <ul class="list-inline">
                                 <li>
                                     <select id="ddMonth" style="width:155px"></select>
                                 </li>
                                 <li>
                                     <select id="ddYear" style="width:115px"></select>
                                 </li>
                             </ul>
                        </div>
                    </li>
                    <li>
                        <button id="btnFilter" class="k-button k-primary" ><span class="k-icon k-i-filter"></span> APPLY</button>
                    </li>
                    <li>
                        <button id="btnCreate" class="k-button k-success-colored"><span class="k-icon k-i-plus"></span> Create</button>
                    </li>
                </ul>
            </div>
            </div>
           <div class="col-md-12">

               <div id="tabOrders">
                            <ul>
                                <li class="k-state-active">
                                    <span style="color:blue">CUSTOMER ORDERS</span>
                                </li>
                                <li>
                                   <span style="color:red">CANCELLED ORDERS</span>
                                </li>
                                
                            </ul>
                            <div>
                                <div id="gvCustmerOrdersList"></div>
                            </div>
                            <div>
                                <div id="gvCustmerOrdersList_Cancelled"></div>
                            </div>
                            
                </div>
             </div>  
           </div>
        </div>
    
    <div id="divAddCustomer" style="display:none">
        <div class="container-fluid">
            <label for="ddAgents">Select Agent: </label> <select id="ddAgents" style="width:250px"></select><br /><br />
            <div id="gvCreateOrders"></div>
       <%-- <div class="row">
            <div class="col-md-12">
                
            </div>
            <div class="col-md-12">
               
            </div>
        </div>--%>
        </div>
    </div>
</asp:Content>
