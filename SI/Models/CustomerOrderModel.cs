﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI
{
    public class CustomerOrderModel
    {
        
        public string CustomerOrderID { get; set; }
        public string CustomerID { get; set; }
        public string SalesAgentID { get; set; }
        public string OrderStatusID { get; set; }
	    public string TransactionCode { get; set; }
        public string OrderDate { get; set; }
        public string TotalAmount { get; set; }
        public string Remarks { get; set; }
        public string ForMonthOf { get; set; }
        public string LeadTime { get; set; }
        public string MarkDeliveredDate { get; set; }
        public string MarkDeliveredByID { get; set; }
        public string DateCancelled { get; set; }
        public string ReasonForCancellation { get; set; }
        public string CancelledByID { get; set; }
        public string Version { get; set; }
        public string IsBadDebt { get; set; }
        public string IsBilled { get; set; }
        public string CreatedByID { get; set; }
	    //public string DateCreated { get; set; }
        public string ModifiedByID { get; set; }
        public string UpdateType { get; set; }

        /***************Description********************/

        public string SalesAgentName { get; set; }
        public string Cust_Company { get; set; }
        public string TIN { get; set; }
        public string CreatedBy { get; set; }


    }
}